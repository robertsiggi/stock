from bs4 import BeautifulSoup
import requests
from requests.exceptions import SSLError

from framework.db.proxies.Ticker import Ticker
from framework.logger import Logger
from framework.storage import Storage


class TickerCrawler:
    base_url = "https://omnieq.com/underlyings/all/full"
    base_url_short = "https://omnieq.com/underlyings"

    logger = Logger("..\..\out", "ticker.log")

    # def __init__(self):
    #    self.url = self.url

    def get_url_infos(self):

        try:
            request = requests.get(self.base_url)
            if request.status_code != 200:
                return

            html = request.text

            soup = BeautifulSoup(html, "html.parser")

            table = soup.find("tbody")
            rows = table.find_all("tr")

            tickers = []
            for row in rows:
                cells = row.find_all("td")

                try:
                    ticker = cells[0].text
                except Exception as exc:
                    self.logger.warning("no ticker")
                    continue

                try:
                    href = cells[0].find("a").attrs["href"]
                except Exception as exc:
                    self.logger.warning("no href for ticker " + ticker)
                    continue

                try:
                    company = cells[1].text
                except Exception as exc:
                    self.logger.warning("no company for ticker " + ticker)
                    continue

                try:
                    market = cells[2].text
                except Exception as exc:
                    self.logger.warning("no market for ticker " + ticker)
                    continue

                try:
                    tickerType = cells[3].text
                except Exception as exc:
                    self.logger.warning("no tickerType for ticker " + ticker)
                    continue

                if ticker == "" or href == "" or company == "" or market == "" or tickerType == "":
                    continue

                ticker = Ticker(ticker=ticker, href=href, company=company, market=market, tickerType=tickerType)
                tickers.append(ticker)

            if len(tickers) > 0:
                self.logger.info("got " + str(len(tickers)) + " symbols")
                Storage.save_data_upsert(tickers, 'StockInfos')
                return tickers

        except SSLError as e:
            return


tickerCrawler = TickerCrawler()
tickers = tickerCrawler.get_url_infos()
