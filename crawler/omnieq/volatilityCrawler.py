import urllib

from bs4 import BeautifulSoup
import requests
from requests.exceptions import SSLError

from framework.db.proxies.Volatility import Volatility
from framework.logger import Logger
from framework.storage import Storage


class VolatilityCrawler:
    base_url = "https://omnieq.com"
    url_suffix = "volatility"

    logger = Logger('..\..\out', 'volatility.log')

    # def __init__(self):
    #    self.url = self.url

    def get_url_infos(self):

        append = True

        counter = 0

        tickers = Storage.read_active_tickers()
        crawled_volatilities = Storage.read_volatilities()

        if append:
            to_crawl_tickers = []
            for ticker in tickers:
                already_crawled = next((volatility_crawled for volatility_crawled in crawled_volatilities if volatility_crawled.ticker == ticker.ticker),
                                       None)
                if already_crawled is not None:
                    continue

                to_crawl_tickers.append(ticker)

        else:
            to_crawl_tickers = tickers

        for ticker in to_crawl_tickers:

            url_ = urllib.parse.urljoin(self.base_url, ticker.href) + "/"
            url_ = urllib.parse.urljoin(url_, self.url_suffix)
            # self.logger.info("crawling ticker " + ticker.ticker + " / " + url_)

            if counter and counter % 10 == 0:
                # print(str(counter) + " / " + str(len(tickers)))
                self.logger.info(str(counter) + " / " + str(len(tickers)))

            counter += 1

            try:
                self._get_url_infos_volatility(ticker.ticker, url_)
            except:
                self.logger.error("problem on crawling " + url_)


    def _get_url_infos_volatility(self, ticker, url):
        try:
            request = requests.get(url)
            if request.status_code != 200:
                self.logger.error("invalid url " + url)
                return
        except SSLError as e:
            self.logger.error("exception url " + url)
            return

        html = request.text

        soup = BeautifulSoup(html, "html.parser")

        table = soup.find("tbody")
        rows = table.find_all("tr")

        volatilities = []

        for row in rows:
            tdContent = row.find("td").text.split()

            volatility = Volatility()
            volatility.ticker = ticker
            volatility.date = tdContent[0]
            volatility.iv = tdContent[1]
            volatility.skew = tdContent[2]
            volatilities.append(volatility)


        Storage.save_data(volatilities, 'StockInfos')


c = VolatilityCrawler()
c.get_url_infos()
