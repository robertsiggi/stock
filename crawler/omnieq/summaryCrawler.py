import urllib

from bs4 import BeautifulSoup
import requests
from requests.exceptions import SSLError

from framework.db.proxies.Summary import Summary
from framework.logger import Logger
from framework.storage import Storage


class SummaryCrawler:
    base_url = "https://omnieq.com"

    logger = Logger('..\..\out', 'summary.log')

    # def __init__(self):
    #    self.url = self.url

    def get_url_infos(self):

        append = True

        counter = 0

        tickers = Storage.read_active_tickers()
        crawled_summaries = Storage.read_summaries()

        if append:
            to_crawl_tickers = []
            for ticker in tickers:
                already_crawled = next((summary_crawled for summary_crawled in crawled_summaries if
                                        summary_crawled.ticker == ticker.ticker),
                                       None)
                if already_crawled is not None:
                    continue

                to_crawl_tickers.append(ticker)

        else:
            to_crawl_tickers = tickers

        for ticker in to_crawl_tickers:

            url = urllib.parse.urljoin(self.base_url, ticker.href)
            self.logger.info("crawling ticker " + ticker.ticker + " / " + url)

            if counter and counter % 10 == 0:
                print(str(counter) + " / " + str(len(to_crawl_tickers)))
                self.logger.info(str(counter) + " / " + str(len(to_crawl_tickers)))

            counter += 1

            if ticker.tickerType == "Stock":
                self._get_url_infos_stock(ticker.ticker, url)
                continue

            if ticker.tickerType == "ETF":
                continue

            if ticker.tickerType == "Index":
                continue

            self.logger.warn("unknown ticker type " + ticker.tickerType + " / ticker " + ticker.ticker)

    def _get_url_infos_stock(self, ticker, url):
        try:
            request = requests.get(url)
            if request.status_code != 200:
                self.logger.error("invalid url " + url)
                return
        except SSLError as e:
            self.logger.error("exception url " + url)
            return

        html = request.text

        soup = BeautifulSoup(html, "html.parser")

        table = soup.find("tbody")
        rows = table.find_all("tr")

        # ticker = ""
        description = ""
        bio = ""
        exchange = ""
        type = ""
        sector = ""
        employees = ""
        nextEarningsDate = ""
        iv = ""
        ivPercentile = ""
        ivRank = ""
        ivSkew = ""
        ivSkewPercentile = ""
        ivSkewSentiment = ""
        optionVolume = ""
        volume = ""
        price = ""
        previousClose = ""
        weekHigh52 = ""
        weekLow52 = ""
        dayMovingAvg50 = ""
        dayMovingAvg200 = ""
        dayAvgVolume10 = ""
        dayAvgVolume20 = ""
        dayAvgVolume30 = ""
        monthAvgVolume3 = ""
        pe = ""
        eps = ""
        outstandingShares = ""
        marketCapitalization = ""
        currency = ""
        updated = ""

        for row in rows:
            th = row.find("th")
            td = row.find("td")

            if th is not None:
                th = th.text
            else:
                th = "Bio"

            if th == "Ticker":
                # we already have ticker
                continue

            if th == "Description":
                try:
                    description = td.text
                except Exception as exc:
                    self.logger.warning("no description for ticket  " + ticker)
                continue

            if th == "Bio":
                try:
                    bio = td.text
                except Exception as exc:
                    self.logger.warning("no bio for ticker " + ticker)
                continue

            if th == "Exchange":
                try:
                    exchange = td.text
                except Exception as exc:
                    self.logger.warning("no exchange for ticker " + ticker)
                continue

            if th == "Type":
                try:
                    type = td.text
                except Exception as exc:
                    self.logger.warning("no type for ticker " + ticker)
                continue

            if th == "Sector":
                try:
                    sector = td.text
                except Exception as exc:
                    self.logger.warning("no sector for ticker " + ticker)
                continue

            if th == "Employees":
                try:
                    employees = td.text
                except Exception as exc:
                    self.logger.warning("no employees for ticker " + ticker)
                continue

            if th == "Next Earnings Date":
                try:
                    nextEarningsDate = td.text
                except Exception as exc:
                    self.logger.warning("no nextEarningsDate for ticker " + ticker)
                continue

            if th == "IV":
                try:
                    iv = td.text
                except Exception as exc:
                    self.logger.warning("no iv for ticker " + ticker)
                continue

            if th == "IV Percentile":
                try:
                    ivPercentile = td.text
                except Exception as exc:
                    self.logger.warning("no ivPercentile for ticker " + ticker)
                    continue

            if th == "IV Rank":
                try:
                    ivRank = td.text
                except Exception as exc:
                    self.logger.warning("no ivRank for ticker " + ticker)
                continue

            if th == "IV Skew":
                try:
                    ivSkew = td.text
                except Exception as exc:
                    self.logger.warning("no ivSkew for ticker " + ticker)
                continue

            if th == "IV Skew Percentile":
                try:
                    ivSkewPercentile = td.text
                except Exception as exc:
                    self.logger.warning("no ivSkewPercentile for ticker " + ticker)
                continue

            if th == "IV Skew Sentiment":
                try:
                    ivSkewSentiment = td.text
                except Exception as exc:
                    self.logger.warning("no ivSkewSentiment for ticker " + ticker)
                continue

            if th == "Option Volume":
                try:
                    optionVolume = td.text
                except Exception as exc:
                    self.logger.warning("no optionVolume for ticker " + ticker)
                continue

            if th == "Volume":
                try:
                    volume = td.text
                except Exception as exc:
                    self.logger.warning("no volume for ticker " + ticker)
                continue

            if th == "Price":
                try:
                    price = td.text
                except Exception as exc:
                    self.logger.warning("no price for ticker " + ticker)
                continue

            if th == "Previous Close":
                try:
                    previousClose = td.text
                except Exception as exc:
                    self.logger.warning("no previousClose for ticker " + ticker)
                continue

            if th == "52-Week High":
                try:
                    weekHigh52 = td.text
                except Exception as exc:
                    self.logger.warning("no weekHigh52 for ticker " + ticker)
                continue

            if th == "52-Week Low":
                try:
                    weekLow52 = td.text
                except Exception as exc:
                    self.logger.warning("no weekLow52 for ticker " + ticker)
                continue

            if th == "50-Day Moving Avg.":
                try:
                    dayMovingAvg50 = td.text
                except Exception as exc:
                    self.logger.warning("no dayMovingAvg50 for ticker " + ticker)
                continue

            if th == "200-Day Moving Avg.":
                try:
                    dayMovingAvg200 = td.text
                except Exception as exc:
                    self.logger.warning("no dayMovingAvg200 for ticker " + ticker)
                continue

            if th == "10-Day Avg. Volume":
                try:
                    dayAvgVolume10 = td.text
                except Exception as exc:
                    self.logger.warning("no dayAvgVolume10 for ticker " + ticker)
                continue

            if th == "20-Day Avg. Volume":
                try:
                    dayAvgVolume20 = td.text
                except Exception as exc:
                    self.logger.warning("no dayAvgVolume20 for ticker " + ticker)
                continue

            if th == "30-Day Avg. Volume":
                try:
                    dayAvgVolume30 = td.text
                except Exception as exc:
                    self.logger.warning("no dayAvgVolume30 for ticker " + ticker)
                continue

            if th == "3-Month Avg. Volume":
                try:
                    monthAvgVolume3 = td.text
                except Exception as exc:
                    self.logger.warning("no monthAvgVolume3 for ticker " + ticker)
                continue

            if th == "PE":
                try:
                    pe = td.text
                except Exception as exc:
                    self.logger.warning("no pe for ticker " + ticker)
                continue

            if th == "EPS":
                try:
                    eps = td.text
                except Exception as exc:
                    self.logger.warning("no eps for ticker " + ticker)
                continue

            if th == "Outstanding Shares":
                try:
                    outstandingShares = td.text
                except Exception as exc:
                    self.logger.warning("no outstandingShares for ticker " + ticker)
                continue

            if th == "Market Capitalization":
                try:
                    marketCapitalization = td.text
                except Exception as exc:
                    self.logger.warning("no marketCapitalization for ticker " + ticker)
                continue

            if th == "Currency":
                try:
                    currency = td.text
                except Exception as exc:
                    self.logger.warning("no currency for ticker " + ticker)
                continue

            if th == "Updated":
                try:
                    updated = td.text
                except Exception as exc:
                    self.logger.warning("no updated for ticker " + ticker)
                continue

        summary = Summary()
        summary.ticker = ticker
        summary.description = description
        summary.bio = bio
        summary.exchange = exchange
        summary.type = type
        summary.sector = sector
        summary.employees = employees
        summary.nextEarningsDate = nextEarningsDate
        summary.iv = iv
        summary.ivPercentile = ivPercentile
        summary.ivRank = ivRank
        summary.ivSkew = ivSkew
        summary.ivSkewPercentile = ivSkewPercentile
        summary.ivSkewSentiment = ivSkewSentiment
        summary.optionVolume = optionVolume
        summary.volume = volume
        summary.price = price
        summary.previousClose = previousClose
        summary.weekHigh52 = weekHigh52
        summary.weekLow52 = weekLow52
        summary.dayMovingAvg50 = dayMovingAvg50
        summary.dayMovingAvg200 = dayMovingAvg200
        summary.dayAvgVolume10 = dayAvgVolume10
        summary.dayAvgVolume20 = dayAvgVolume20
        summary.dayAvgVolume30 = dayAvgVolume30
        summary.monthAvgVolume3 = monthAvgVolume3
        summary.pe = pe
        summary.eps = eps
        summary.outstandingShares = outstandingShares
        summary.marketCapitalization = marketCapitalization
        summary.currency = currency
        summary.updated = updated

        print(summary.ticker)
        Storage.save_data([summary], 'StockInfos')


c = SummaryCrawler()
c.get_url_infos()
