import logging
import os
from logging.handlers import TimedRotatingFileHandler
from pathlib import Path


class Logger:
    statusLogger: logging = None
    errorLogger: logging = None
    tweetLogger: logging = None

    def __init__(self, log_path, filename):
        Path(log_path).mkdir(parents=True, exist_ok=True)
        self.set_status_logger_instance(os.path.join(log_path, filename))

    def set_status_logger_instance(self, log_path):
        self.statusLogger = logging.getLogger('statusLogger')
        self.statusLogger.setLevel(logging.INFO)
        statusHandler = TimedRotatingFileHandler(log_path, when="midnight", interval=1, backupCount=5)
        statusHandler.setLevel(logging.INFO)
        statusHandler.setFormatter(logging.Formatter("%(asctime)s | %(levelname)s | %(message)s"))
        self.statusLogger.addHandler(statusHandler)

    def info(self, msg):
        print(msg)
        self.statusLogger.info(msg)

    def warning(self, msg):
        print(msg)
        self.statusLogger.warning(msg)

    def error(self, msg, informInStatusLog=True):
        print(msg)
        self.statusLogger.error(msg)

    def critical(self, msg, informInStatusLog=True):
        print(msg)
        self.statusLogger.critical(msg)
