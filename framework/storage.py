import json
import os
from pathlib import Path
from typing import List

from sqlalchemy.orm import Session

from framework.db.factories.DBManagerFactory import DBManagerFactory
from framework.db.managers.DBManager import DBManager
from framework.db.proxies.Summary import Summary
from framework.db.proxies.Ticker import Ticker
from framework.db.proxies.Volatility import Volatility

path = os.path.join("..\..\out\data")


class Storage:

    @staticmethod
    def save_data(data, ticker):
        Storage._save_as_db_record(data, ticker)

    @staticmethod
    def save_data_upsert(data, ticker):
        Storage._save_as_db_record(data, ticker, True)

    @staticmethod
    def read_data(ticker, cls, filter=None, limit=None, order=None):
        dbManager = DBManagerFactory.getManager(ticker)
        session = dbManager.getSession()
        query = session.query(cls)
        if filter is not None:
            query = query.filter_by(*filter)
        if order is not None:
            query = query.order_by(order)
        if limit is not None:
            query = query.limit(limit)

        return query.all()

    @staticmethod
    def read_volatilities():
        dbManager = DBManagerFactory.getManager(DBManager.MAIN_DATABASE)
        session = dbManager.getSession()
        volatilities = session.query(Volatility)

        return volatilities.all()

    @staticmethod
    def read_summaries():
        dbManager = DBManagerFactory.getManager(DBManager.MAIN_DATABASE)
        session = dbManager.getSession()
        summaries = session.query(Summary)

        return summaries.all()

    @staticmethod
    def read_active_tickers():
        dbManager = DBManagerFactory.getManager(DBManager.MAIN_DATABASE)
        session = dbManager.getSession()
        tickers = session.query(Ticker).filter(Ticker.active == True)

        return tickers.all()

    @staticmethod
    def has_historical_price_for_ticker_for_date(ticker, date):
        dbManager = DBManagerFactory.getManager(ticker)
        session = dbManager.getSession()
        tickers = session.query(Ticker).filter(Ticker.active == True)

        return tickers.all()

    @staticmethod
    def read_tickers():
        dbManager = DBManagerFactory.getManager(DBManager.MAIN_DATABASE)
        session = dbManager.getSession()
        tickers = session.query(Ticker)

        return tickers.all()

    # ticker: AAPL
    # dataType: fundamental OR stock OR option
    # data: data array
    @staticmethod
    def save(tickers, dataType):
        Storage._save_as_db_record(tickers, dataType)

    def _show_in_log(ticker, dataType, data):
        print(ticker)
        print(dataType)
        for singleData in data:
            print(singleData)

    def _save_as_json(ticker, dataType, data):
        Path(path).mkdir(parents=True, exist_ok=True)

        if isinstance(data, list):
            jsonString = json.dumps([data.__dict__ for dataSingle in data])
        else:
            jsonString = json.dumps([data.__dict__])

        filename = Storage._get_filename(ticker, dataType)
        if os.path.exists(filename):
            os.remove(filename)

        f = open(filename, "a")
        f.write(jsonString)
        f.close()

    @staticmethod
    def _save_as_db_record(objects: List[any], ticker, upsert=False):
        print('saving list of ' + str(len(objects)) + ' objects')
        dbManager = DBManagerFactory.getManager(ticker)
        session = dbManager.getSession()
        if not upsert:
            session.add_all(objects)
        else:
            print('upsert option selected: performing update or insert on selected objects')
            Storage._save_as_db_record_upsert(objects, session)
        session.commit()

    @staticmethod
    def _save_as_db_record_upsert(objects: List[any], session: Session):
        i = 0
        updated = 0
        inserted = 0
        failed = 0
        numObj = len(objects)
        for object in objects:
            if i % 10 == 0:
                print(str(i) + '/' + str(numObj) + ' objects processed')
            proxyType = type(object)
            filter = proxyType.getFilterForUpsert(object)
            foundObj = session.query(proxyType).filter_by(**filter).first()
            try:
                if foundObj:
                    object.id = foundObj.id
                    session.merge(object)
                    updated += 1
                else:
                    session.add(object)
                    inserted += 1
            except:
                failed += 1
            i += 1
        session.commit()
        print(str(updated) + ' updated - ' + str(inserted) + ' inserted - ' + str(failed) + ' failed')

    def _get_filename(ticker, dataType):
        if (dataType != ''):
            return os.path.join(path, ticker + "_" + dataType + ".json")

        return os.path.join(path, ticker + ".json")
