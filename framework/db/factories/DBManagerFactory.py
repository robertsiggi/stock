from framework.db.managers.DBManager import DBManager


class DBManagerFactory:
    _dbManager = {}

    @staticmethod
    def getManager(ticker: str) -> DBManager:
        if not ticker in DBManagerFactory._dbManager:
            DBManagerFactory._dbManager[ticker] = {}
            DBManagerFactory._dbManager[ticker] = DBManager(ticker)

        return DBManagerFactory._dbManager[ticker]
