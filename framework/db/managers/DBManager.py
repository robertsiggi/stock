import os
import sqlalchemy as db
from sqlalchemy.engine import Engine
from sqlalchemy.orm import sessionmaker, Session

from framework.Environment import Environment
from framework.db.proxies.Fundamental import Fundamental
from framework.db.proxies.Option import Option
from framework.db.proxies.StockHistoricalPrice import StockHistoricalPrice
from framework.db.proxies.StockHistoricalPriceDayCrawled import StockHistoricalPriceDayCrawled
from framework.db.proxies.Summary import Summary
from framework.db.proxies.Ticker import Ticker
from framework.db.proxies.Volatility import Volatility


class DBManager:

    MAIN_DATABASE = "StockInfos"

    _session = None

    symbolPath: str
    dbFilePath: str

    def __init__(self, ticker):
        dataPath = Environment.getDataPath()
        if not os.path.exists(dataPath):
            os.makedirs(dataPath)

        symbolPath = os.path.join(dataPath, ticker[0].lower(), ticker)
        self.symbolPath = symbolPath

        if ticker == self.MAIN_DATABASE:
            self.dbFilePath = os.path.join(dataPath, ticker + ".db")
        else:
            self.dbFilePath = os.path.join(symbolPath,ticker + ".db")
            if not os.path.exists(symbolPath):
                os.makedirs(symbolPath)

        if (ticker == self.MAIN_DATABASE):
            self.createTickerDB()
            return
        self.createTablesForSymbol()

    def getConnectionStr(self) -> str:
        return "sqlite:///" + self.dbFilePath

    def getDBEngine(self) -> Engine:
        return db.create_engine(self.getConnectionStr())

    def getSession(self) -> Session:
        if not self._session:
            Session = sessionmaker(self.getDBEngine())
            self._session = Session()
        return self._session

    def createTickerDB(self):
        engine = self.getDBEngine()

        ticker = Ticker()
        ticker.createTable(self.getDBEngine())

        fundamental = Fundamental()
        fundamental.createTable(engine)

        summary = Summary()
        summary.createTable(engine)

        volatility = Volatility()
        volatility.createTable(engine)

    def createTablesForSymbol(self):
        engine = self.getDBEngine()

        option = Option()
        option.createTable(engine)

        stock_historical_price = StockHistoricalPrice()
        stock_historical_price.createTable(engine)

        stock_historical_price_day_crawled = StockHistoricalPriceDayCrawled()
        stock_historical_price_day_crawled.createTable(engine)

