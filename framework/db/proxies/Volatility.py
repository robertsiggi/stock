from sqlalchemy import Column, String
from sqlalchemy.ext.declarative import declarative_base
from framework.db.proxies.DBObject import DBObject
Base = declarative_base()


class Volatility(DBObject, Base):
    __tablename__ = "volatility"

    ticker = Column(String)
    date = Column(String)
    iv = Column(String)
    skew = Column(String)

    def createTable(self, engine):
        Base.metadata.create_all(engine)
