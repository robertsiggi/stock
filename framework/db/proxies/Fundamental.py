from sqlalchemy.ext.declarative import declarative_base
from framework.db.proxies.DBObject import DBObject
Base = declarative_base()


class Fundamental(DBObject, Base):
    __tablename__ = "fundamental"

    def createTable(self, engine):
        Base.metadata.create_all(engine)