from sqlalchemy import Column, String
from sqlalchemy.ext.declarative import declarative_base
from framework.db.proxies.DBObject import DBObject
Base = declarative_base()


class Summary(DBObject, Base):
    __tablename__ = "summary"

    ticker = Column(String)
    description = Column(String)
    bio = Column(String)
    exchange = Column(String)
    type = Column(String)
    sector = Column(String)
    employees = Column(String)
    nextEarningsDate = Column(String)
    iv = Column(String)
    ivPercentile = Column(String)
    ivRank = Column(String)
    ivSkew = Column(String)
    ivSkewPercentile = Column(String)
    ivSkewSentiment = Column(String)
    optionVolume = Column(String)
    volume = Column(String)
    price = Column(String)
    previousClose = Column(String)
    weekHigh52 = Column(String)
    weekLow52 = Column(String)
    dayMovingAvg50 = Column(String)
    dayMovingAvg200 = Column(String)
    dayAvgVolume10 = Column(String)
    dayAvgVolume20 = Column(String)
    dayAvgVolume30 = Column(String)
    monthAvgVolume3 = Column(String)
    pe = Column(String)
    eps = Column(String)
    outstandingShares = Column(String)
    marketCapitalization = Column(String)
    currency = Column(String)
    updated = Column(String)

    def createTable(self, engine):
        Base.metadata.create_all(engine)
