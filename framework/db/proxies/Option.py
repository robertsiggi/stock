from sqlalchemy.ext.declarative import declarative_base
from framework.db.proxies.DBObject import DBObject
Base = declarative_base()


class Option(DBObject, Base):
    __tablename__ = "option"

    # href = Column(String)
    # company = Column(String)
    # market = Column(String)

    def createTable(self, engine):
        Base.metadata.create_all(engine)