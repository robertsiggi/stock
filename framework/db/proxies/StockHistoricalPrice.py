from sqlalchemy import Column, String
from sqlalchemy.ext.declarative import declarative_base
from framework.db.proxies.DBObject import DBObject
Base = declarative_base()


class StockHistoricalPrice(DBObject, Base):
    __tablename__ = "stockhistoricalprice"

    timestamp = Column(String)
    open = Column(String)
    close = Column(String)
    high = Column(String)
    low = Column(String)
    volume = Column(String)
    wmap = Column(String)

    def createTable(self, engine):
        Base.metadata.create_all(engine)
