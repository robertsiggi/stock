import datetime

from sqlalchemy import Column, String, DateTime, Boolean
from sqlalchemy.ext.declarative import declarative_base
from framework.db.proxies.DBObject import DBObject
Base = declarative_base()


class Ticker(DBObject, Base):
    __tablename__ = "ticker"

    ticker = Column(String)
    href = Column(String)
    company = Column(String)
    market = Column(String)
    tickerType = Column(String)
    active = Column(Boolean, default=False)

    @staticmethod
    def getFilterForUpsert(compareObj):
        return {
            'ticker': compareObj.ticker
        }

    def createTable(self, engine):
        Base.metadata.create_all(engine)