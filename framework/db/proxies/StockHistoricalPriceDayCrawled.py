from sqlalchemy import Column, String
from sqlalchemy.ext.declarative import declarative_base
from framework.db.proxies.DBObject import DBObject
Base = declarative_base()


class StockHistoricalPriceDayCrawled(DBObject, Base):
    __tablename__ = "stockhistoricalpricedaycrawled"

    day = Column(String)


    def createTable(self, engine):
        Base.metadata.create_all(engine)
