import os
from pathlib import Path


class Environment:
    @staticmethod
    def getBasePath():
        return Path(__file__).parent.parent

    @staticmethod
    def getDataPath():
        return os.path.join(Environment.getBasePath(), 'out', 'data')


