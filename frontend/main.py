import matplotlib.pyplot as plt

from framework.db.proxies.Volatility import Volatility
from framework.storage import Storage

ticker = "GME"

objects = Storage.read_data(ticker, Volatility, order=Volatility.date.desc(), limit=50)
dates = []
ivs = []
skews = []
for object in objects:
    dates.append(object.date)
    ivs.append(float(object.iv.replace('%', '')))
    skews.append(float(object.skew.replace('%', '')))

chart1 = plt.plot(dates, ivs, color='Blue')
chart2 = plt.plot(dates, skews, color='Red')
plt.title(ticker)
plt.xlabel('Date')
plt.ylabel('iv / Skew')
plt.show()